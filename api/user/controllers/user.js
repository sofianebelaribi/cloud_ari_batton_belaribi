'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
const {parseMultipartData,convertRestQueryParams, buildQuery} = require('strapi-utils');
//override sanitizeEntity in order to hide __v & _id fields
const {sanitizeEntity} = require('../../../plugins/index');
//import moment.js library
const moment = require('moment');

module.exports = {

  /**
   *
   * @route GET /user
   * @response returns all users
   */
  async find(ctx) {
    let entities;

    if (ctx.query._q) {
      entities = await strapi.services.user.search(ctx.query);
    } else {
      entities = await strapi.services.user.find(ctx.query);
    }
    return entities.map(entity => {
        //change date format display
        entity.birthDay = moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY");
        return sanitizeEntity(entity, {model: strapi.models.user});
      }
    );
  },

  /**
   *
   * @route GET /user/page
   * @filter gt || eq
   * @response returns all users by age with a filter
   */
  async findByAge(ctx) {
    let entities;

    if(ctx.query.gt >= 0 || ctx.query.eq >= 0){
      entities = await strapi.services.user.findByAge(ctx.query);
    }
    else {
      return ctx.status = 400;
    }
    return entities.map(entity => {
      entity.birthDay = moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY");

      return sanitizeEntity(entity, {model: strapi.models.user});
    });
  },

  /**
   *
   * @route GET /user/search{name}
   * @response returns user with name={name}
   */
  async findByName(ctx) {
    let entities;
    entities = await strapi.services.user.findByName(ctx.query);

    return entities.map(entity => {
      entity.birthDay = moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY");

      return sanitizeEntity(entity, {model: strapi.models.user});
    });
  },

  /**
   *
   * @route GET /user/page
   * @filter gt || eq
   * @response returns all users by age with a filter
   */
  async findByNearest(ctx) {
    let entities;

    entities = await strapi.services.user.findByNearest(ctx.query);

    // return entities.map(entity => {
    //   entity.birthDay = moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY");
    //   return sanitizeEntity(entity, {model: strapi.models.user});
    // });
    return entities;
  },

  /**
   *
   * @route GET /user/{id}
   * @response returns user with id={id}
   */
  async findOne(ctx) {
    let entity;

    if(ctx.params.id.match(/^[0-9a-fA-F]{24}$/)) {
      entity = await strapi.services.user.findOne(ctx.params);
      if(entity != null){
        entity.birthDay = moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY")
      }
    }
    else {
      ctx.status = 404;
    }

    //change date format display
    return sanitizeEntity(entity, {model: strapi.models.user});
  },

  /**
   *
   * @route POST /user
   * @response adds a user
   */
  async create(ctx) {
    ctx.status = 201;
    let entity;
    //switch date input string to date format
    ctx.request.body.birthDay = moment(ctx.request.body.birthDay, "MM/DD/YYYY").toDate()

    if (ctx.is('multipart')) {
      const {data, files} = parseMultipartData(ctx);
      entity = await strapi.services.user.create(data, {files});
    } else {
      entity = await strapi.services.user.create(ctx.request.body);
    }
    //change date format display
    entity.birthDay = moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY")

    return sanitizeEntity(entity, {model: strapi.models.user});
  },

  /**
   *
   * @route PUT /user
   * @response replaces whole collection
   */
  async updateAll(ctx) {
    ctx.status = 201;
    let entities;

    //switch date input string to date format for each entity
    ctx.request.body.map(entity => {
      entity.birthDay = moment(entity.birthDay, "MM/DD/YYYY").toDate();
    });


    if (ctx.is('multipart')) {
      const {data, files} = parseMultipartData(ctx);
      entities = await strapi.services.user.updateAll(ctx.params, data, {
        files,
      });
    } else {
      entities = await strapi.services.user.updateAll(
        ctx.params,
        ctx.request.body
      );
    }

    return entities.map(entity => {
      //change date format display
      entity.birthDay = moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY");
      return sanitizeEntity(entity, {model: strapi.models.user});
    });
  },

  /**
   *
   * @route DELETE /user
   * @response deletes whole collection
   */
  async deleteAll() {
    await strapi.services.user.deleteAll();
    return {};
  },
};
