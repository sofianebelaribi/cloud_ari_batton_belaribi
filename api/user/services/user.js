'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/3.0.0-beta.x/concepts/services.html#core-services)
 * to customize this service
 */
const moment = require('moment');

module.exports = {

  //override PUT /user method
  async updateAll(params, data, { files } = {}) {
    await strapi.query('user').model.deleteMany({});
    await strapi.query('user').model.insertMany(data);
    return strapi.query('user').find()
  },

  //override get /user method in order to filter on ?page={nb}
  find(params) {
    if (params.hasOwnProperty('page')) {
      const start_index = params.page *100;
      params = {_limit: 100, _start: start_index};
    }
    return strapi.query('user').find(params);//try with no populate
  },

  //override get /user/age method in order to filter on gt & eq
  findByAge(params) {
    let minDate;
    let maxDate;
    const start_index = params.page *100 || 0;

    if (params.hasOwnProperty('gt')) {
      minDate = moment().subtract(params.gt, 'years');

      params = {birthDay_lte: minDate.toISOString(),_limit: 100, _start: start_index};
    }
    else if (params.hasOwnProperty('eq')) {
      minDate = moment().subtract(params.eq, 'years');
      maxDate = moment().subtract(Number(params.eq)+1, 'years');

      params = {birthDay_lte: minDate.toISOString(),birthDay_gte: maxDate.toISOString(),_limit: 100, _start: start_index};
    }

    return strapi.query('user').find(params);//try with no populate
  },

//override get /user method in order to filter on ?page={nb}
  //GET /user/search?name=toto -> retourne les 100 premiers utilisateurs dont le nom est "toto"
  findByName(params) {
    const start_index = params.page *100 || 0;
    params = {$or: [
        {firstName: params.term},
        {lastName: params.term}
      ]};
    console.log(params)
    return strapi.query('user').model.find(params).limit(100).skip(start_index);
  },
  //override get /user/nearest method in order to filter on lon & lat
  findByNearest(params) {
    //GET /user/nearest?lat=43.12&lon=5.97 -> retourne les 10 utilisateurs les plus proches
    console.log(params)
    if(params.lon & params.lat){
      params = {birthDay_lte: minDate.toISOString(),_limit: 100, _start: start_index};
    }

    //EXEMPLE : query.where('loc').nearSphere({ center: [10, 10], maxDistance: 5 });

    // console.log(
    //   strapi.query('user').model.where('loc').near({center : [10,10]})
    // );

    // return strapi.query('user').find(params);//try with no populate
    // return strapi.query('user').model.find({ firstName: "Antoine" })

    // return strapi.query('user').model.where('loc').near({center : [10,10]})
    /** return strapi.query('user').model.where('loc').near({center : [10,10]})*/
    return []


    //  https://stackoverflow.com/questions/25734092/query-locations-within-a-radius-in-mongodb
  },

  //override delete /user method in order to clear collection
  async deleteAll() {
    return await strapi.query('user').model.deleteMany({})
  },
};


