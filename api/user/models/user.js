'use strict';

/**
 * Lifecycle callbacks for the `user` model.
 */

module.exports = {
  // Before saving a value.
  // Fired before an `insert` or `update` query.
  // beforeSave: async (model, attrs, options) => {},
/**  beforeSave: async (model, attrs, options) => {
    console.log("beforeSave")
    console.log("----------- avant d'enregistrer le user -------")
    console.log(model.birthDay)
    console.log(model.birthDay = "aaaa/aaa/aaa")
  },*/

  // After saving a value.
  // Fired after an `insert` or `update` query.
  // afterSave: async (model, response, options) => {},
/**  afterSave: async (model, attrs, options) => {
    console.log("afterSave")
    console.log("----------- apres enregistrer le user -------")
    console.log(model.birthDay)
    console.log(model.birthDay = "aaaa/aaa/aaa")
  },*/
  // Before fetching a value.
  // Fired before a `fetch` operation.
  // beforeFetch: async (model, columns, options) => {},

  // After fetching a value.
  // Fired after a `fetch` operation.
  // afterFetch: async (model, response, options) => {},
/**afterFetch: async (model, response, options) => {
  console.log("afterFetch");
  console.log("------- apres afficher le user -------");
  console.log(response)
  console.log("---------------")
  console.log(response.birthDay);
  // console.log(response.birthDay = 'bbbb/bbb/bbbb');
},*/
  // Before fetching all values.
  // Fired before a `fetchAll` operation.
  // beforeFetchAll: async (model, columns, options) => {},

  // After fetching all values.
  // Fired after a `fetchAll` operation.
  // afterFetchAll: async (model, response, options) => {},
/**  afterFetchAll: async (model, columns, options) => {
    console.log("afterFetchAll")
    console.log("------- avant d'afficher les users -------")
    // console.log(columns)
    console.log("---------------")
    columns.map(entity => {
      console.log(entity.birthDay);
      console.log(typeof entity.birthDay);
      console.log(moment(entity.birthDay, "YYYY-MM-DD").toISOString());
      console.log(entity.birthDay = moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY"));
      console.log(typeof moment(entity.birthDay, "YYYY-MM-DD").format("MM/DD/YYYY"));
      entity.birthDay = new Date('1995-12-17T03:24:00');
      entity.birthDay = new Date('1995-12-17T03:24:00');
    })

    console.log(columns)
  },*/
  // Before creating a value.
  // Fired before an `insert` query.
  // beforeCreate: async (model, attrs, options) => {},
/**  beforeCreate: async (model, attrs, options) => {
    console.log("beforeCreate")
    console.log("----------- avant d'enregistrer le user -------")
    console.log("model : " + model)
    console.log("attrs : " + attrs)
    console.log("options : " + options)
    console.log(typeof model.birthDay)
    console.log(model.birthDay)
    console.log(model.birthDay = "14/02/1993")
  },*/

  // After creating a value.
  // Fired after an `insert` query.
  // afterCreate: async (model, attrs, options) => {},
/**  afterCreate: async (model, attrs, options) => {
    console.log("afterCreate")
    console.log("----------- apres d'enregistrer le user -------")
    console.log("model : " + model)
    console.log("attrs : " + attrs)
    console.log("options : " + options)
    console.log(typeof model.birthDay)
    console.log(model.birthDay)
    console.log(model.birthDay = "14/02/1993")
    console.log(attrs.birthDay = "14/02/1993")
  },*/
  // Before updating a value.
  // Fired before an `update` query.
  // beforeUpdate: async (model, attrs, options) => {},

  // After updating a value.
  // Fired after an `update` query.
  // afterUpdate: async (model, attrs, options) => {},

  // Before destroying a value.
  // Fired before a `delete` query.
  // beforeDestroy: async (model, attrs, options) => {},

  // After destroying a value.
  // Fired after a `delete` query.
  // afterDestroy: async (model, attrs, options) => {}
};
