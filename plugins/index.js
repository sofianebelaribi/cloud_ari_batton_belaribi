'use strict';

/**
 * Export shared utilities
 */

const sanitizeEntity = require('./sanitize-entity');

module.exports = {
  sanitizeEntity,
};
